# Ubicast Uploader

AUBU was initially built with the goal of providing an easy way to bulk upload files to Ubicast MediaServer using their REST API.

## Installation / Get Started

### Pre-requisites

* Docker (https://www.docker.com/get-docker)
* Docker Compose(Windows and MacOS already include this on the standard Docker install) (https://docs.docker.com/compose/install/)

### Step by step

#### Linux and MacOS

* cd ./build && ./bash_init.sh
* /html - **(Source code is here)**
* **Browse to** http://127.0.0.1:8080/public
* **Username:** admin **Password:** ubicastupload

#### Windows

* ./build && ./win_init.ps1
* /html - **(Source code is here)**
* **Browse to** http://127.0.0.1:8080/public
* **Username:** admin **Password:** ubicastupload

# Documentation

Make the following changes to the code:
* /html/public/main.php: Edit $API_KEY(i.e. "xxx-xxx-xxx-xxx-xxx") and $url(i.e. "https://xxx.xxx.xxx/api/v2/channels/tree/?api_key=".$API_KEY;)
* /html/public/submit.php: Edit $url(ie. 'https://xxx.xxx.xxx/api/v2/medias/add/';) and api_key(i.e. "1234-1234-1234-1234-1234",)

Upload flow:	
* Client PC upload file to container	
* Container receive the file and stores on /html/public/temp/	
* Container performs the upload from the temp folder to the Ubicast MediaServer.	

# References
* https://vincentgarreau.com/particles.js/
* https://dribbble.com/shots/4249163-Animated-login-form-avatar
* https://cookieconsent.insites.com/

# License
AUBU is Apache 2.0.

## Screenshots

![Screenshot](screenshot_1.png)
![Screenshot](screenshot_2.png)
