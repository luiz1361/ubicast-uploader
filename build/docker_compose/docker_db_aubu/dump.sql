USE `mysql`;

LOCK TABLES `user` WRITE;
INSERT INTO `user` VALUES ('localhost','admin','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','','','','',0,0,0,0,'mysql_native_password','*6BB4837EB74329105EE4568DDA7DC67ED2CA2AD9','N','2018-06-26 08:27:17',NULL,'N');
UNLOCK TABLES;

DROP DATABASE IF EXISTS `aubu`;

CREATE DATABASE `aubu` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

GRANT ALL PRIVILEGES ON aubu.* TO 'admin'@'%';

USE `aubu`;

DROP TABLE IF EXISTS `person`;

CREATE TABLE `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CharSet=utf8mb4;

LOCK TABLES `person` WRITE;
INSERT INTO `person`
SET username = 'admin',
    password = '$2y$10$4uulkU9ZhtQp7.gwHjyZ.eSfGRUQ9wAipG9TeHrwTMnWCjnAj/dFS';
UNLOCK TABLES;



