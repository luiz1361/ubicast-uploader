<?php 
    include("includes/session.php");
?>

<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Awesome Ubicast Bulk Uploader</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
        <!-- jQuery library -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js" integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i" crossorigin="anonymous"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.2/js/bootstrap.min.js"></script>
        <!-- Bootstrap Toggle -->
        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
        <!-- FontAwesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
        <style>
            body {
                background-color: #f1f1f1;
            }
            .upload-form {
                width: 600px;
                margin: 50px auto;
            }
            .upload-form form {
                margin-bottom: 15px;
                background: #f7f7f7;
                box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
                padding: 30px;
            }
            .upload-form h2 {
                margin: 0 0 15px;
            }
            .form-control, .btn {
                min-height: 38px;
                border-radius: 2px;
            }
            .btn {        
                font-size: 15px;
                font-weight: bold;
                text-align: center;
            }
            .btn {
                border: 1px solid #CED4DA;
                color: gray;
                background-color: white;
                padding: 8px 20px;
                font-size: 20px;
                font-weight: bold;
            }
        </style>
    </head>
    <body>
    
        <div class="upload-form">
            <form method="post" action="submit.php" enctype="multipart/form-data" encoding="multipart/form-data">
                <p class="h1 text-center" ><b><i class="fas fa-file-upload"></i> Awesome Ubicast Bulk Uploader</p></b>
                <p></p>
                <select name="channel" class="form-control" required>
                <option value="" disabled selected>Select the destination channel</option>
                    <?php
                        $API_KEY ="";
                        $url = "".$API_KEY;
                        $data = @file_get_contents($url);
                        $jsondata = json_decode($data,true);
                        if(is_array($jsondata) && $jsondata['success'] == "OK")
                        {
                            foreach ($jsondata['channels'] as $subchannels) {
                                echo "<option value=" . $subchannels['oid'] . ">" . $subchannels['title'] . "</option>";
                                foreach ($subchannels['channels'] as $subchannels2) {
                                    echo "<option value=" . $subchannels2['oid'] . ">&emsp;-&emsp;" . $subchannels2['title'] . "</option>";
                                    foreach ($subchannels2['channels'] as $subchannels3) {
                                        echo "<option value=" . $subchannels3['oid'] . ">&emsp;&emsp;-&emsp;" . $subchannels3['title'] . "</option>";
                                        foreach ($subchannels3['channels'] as $subchannels4) {
                                            echo "<option value=" . $subchannels4['oid'] . ">&emsp;&emsp;&emsp;-&emsp;" . $subchannels4['title'] . "</option>";
                                            foreach ($subchannels4['channels'] as $subchannels5) {
                                                echo "<option value=" . $subchannels5['oid'] . ">&emsp;&emsp;&emsp;&emsp;-&emsp;" . $subchannels5['title'] . "</option>";
                                            } 
                                        } 
                                    }
                                }
                            }
                            // echo '<pre>'; print_r($jsondata);echo '</pre>';
                        }  
                    ?>
                </select>
                <p></p>
                <div class="text-center">
                    <input name="validated" type="checkbox" data-toggle="toggle" data-on="PUBLISH ON" data-off="PUBLISH OFF" value="yes" checked>
                    <input name="ocr" type="checkbox" data-toggle="toggle" data-on="OCR ON" data-off="OCR OFF" value="yes">
                    <input name="slide" type="checkbox" data-toggle="toggle" data-on="SLIDE DETECTION ON" data-off="SLIDE DETECTION OFF" value="yes">
                </div>
                </script>
                <p></p>
                <div class="alert alert-info alert-dismissible fade show" role="info">
                    <strong>Please Read!</strong> Maximum upload of 10 files and 5GB in total. <abbr title="Optical character recognition" class="initialism">OCR</abbr> and Slide Detection are disabled by default, but you can always enable them.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <hr></hr>
                <div class="form-group fieldGroup">                
                    <div class="text-center">
                        <input type="submit" name="submit" class="btn btn-primary " value="UPLOAD"/>
                    </div>
                    <p></p>
                    <div class="custom-file">
                        <input type="file" class="file-input form-control" name="file[]" id="file" data-multiple-caption="{count} files selected"  required/>
                        
                        <input type="text" name="title[]" class="form-control" placeholder="Video Title ie. What is ARM?" required/>   
                        <input type="text" name="chapter[]" class="form-control" placeholder="Video Chapters ie. Lecturer's Brief and Introduction to MBA Finance (00:00);Outline of Finance Modules 1-4 (06:25);" />
                        <p></p> 
                        <div class="text-center">              
                            <a href="javascript:void(0)" class="btn btn-primary addMore">+</a>
                        </div>
                    </div>
                </div>
            </form>
            <div class="form-group fieldGroupCopy" style="display: none;">
                <hr></hr>
                <div class="custom-file">
                    <input type="file" class="file-input form-control" name="file[]" id="file" data-multiple-caption="{count} files selected"  required/>
                    
                    <input type="text" name="title[]" class="form-control" placeholder="Video Title ie. What is ARM?" required/>
                    <input type="text" name="chapter[]" class="form-control" placeholder="Video Chapters ie. Lecturer's Brief and Introduction to MBA Finance (00:00);Outline of Finance Modules 1-4 (06:25);" />
                    <p></p>
                    <div class="text-center">                
                        <a href="javascript:void(0)" class="btn btn-primary remove">-</a>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <div class="modal" id="ModalCenter" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">UPLOAD PROGRESS</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="progress">
                <div class="progress-bar progress-bar-striped bar progress-bar-animated" role="progressbar"></div>
                <div class="progress-bar progress-bar-striped percent progress-bar-animated" role="progressbar">0%</div>
            </div>
        <p></p>
        <div id="status"></div>
        </div>
        <p></p>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">CLOSE</button>
        </div>
        </div>
    </div>
    </div>
</html> 

<script>
    $('.modal').appendTo("body");
    $('.modal').on('hidden.bs.modal', function () { 
        location.reload();
    });
</script>
<script>
    $(document).ready(function(){
        var maxGroup = 10;
        $(".addMore").click(function(){
            if($('body').find('.fieldGroup').length < maxGroup){
                var fieldHTML = '<div class="form-group fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
                $('body').find('.fieldGroup:last').after(fieldHTML);
            }else{
                alert('Maximum upload of '+maxGroup+' files are allowed.');
            }
        });
        $("body").on("click",".remove",function(){ 
            $(this).parents(".fieldGroup").remove();
        });
    });
</script>
<script>
    $(function() {
        var bar = $('.bar');
        var percent = $('.percent');
        var status = $('#status');
        var statuscomplete = $('#ModalCenter');
        $('form').ajaxForm({
            beforeSend: function() {
                statuscomplete.modal();
                status.html("Upload in progress, please wait. We are not liable for any broken screens as a result of waiting.");
                var percentVal = '0%';
                bar.width(percentVal);
                percent.html(percentVal);
            },
            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal);
                percent.html(percentVal);
            },
            complete: function(xhr) {
                status.html(xhr.responseText);
            }
        });
    }); 
</script>
