<?php
include("includes/session.php");
require("includes/parser.php");

$url = '';
    if(isset($_POST['submit'])){
        $titleArr = $_POST['title'];
        $chapterArr = $_POST['chapter'];
        $channel = $_POST['channel'];
        $fileArr = $_FILES['file'];
        if(!isset($_POST["ocr"])){
            $ocr = "no";
        }
        else{
            $ocr = $_POST['ocr'];
        }
        if(!isset($_POST["slide"])){
            $slide = "no";
        }
        else{
            $slide = $_POST['slide'];
        }
        if(!isset($_POST["validated"])){
            $validated = "no";
        }
        else{
            $validated = $_POST['validated'];
        }
        if(!empty($titleArr)){
            echo "<p></p>";
            echo "<table class='table' border='1'>";
                echo "<thead class='thead-dark'>";
                    echo "<tr>";
                        echo "<th scope='col'>Video Title</th>";
                        echo "<th scope='col'>Upload Result</th>";
                    echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
            for($i = 0; $i < count($titleArr); $i++){
                if(!empty($titleArr[$i])){
                    $title = $titleArr[$i];
                    $bytes = openssl_random_pseudo_bytes(15);
                    $hash = bin2hex($bytes);
                    $file = $fileArr;
                    $tmp_filename = $file['tmp_name'][$i];
                    $filename = $file['name'][$i];
                    mkdir('temp/' . $hash, 0777, true);
                    $handle = fopen("temp/" . $hash . "/" . 'metadata.json' , 'wb');
                    fwrite($handle, chapterParser($chapterArr[$i]));
                    fclose($handle);
                    print($publish);
                    $handle = fopen($tmp_filename, "rb");
                    $data = fread($handle, filesize($tmp_filename));
                    file_put_contents("temp/" . $hash . "/" . $filename, $data);
                    $zip = new ZipArchive;
                    if ($zip->open("temp/" . $hash . "/" . 'archive.zip', ZipArchive::CREATE)){
                        $zip->addFile("temp/" . $hash . "/" . $filename, $filename);
                        $zip->addFile("temp/" . $hash . "/" . "metadata.json", 'metadata.json');
                        $zip->close();
                    }
                    $ch = curl_init();
                    curl_setopt_array ( $ch, array (
                        CURLOPT_URL             => $url,
                        CURLOPT_TIMEOUT         => 30,
                        CURLOPT_CUSTOMREQUEST   => "POST",
                        CURLOPT_RETURNTRANSFER  => "true",
                        CURLOPT_TIMEOUT         => 5,
                        CURLOPT_CONNECTTIMEOUT  => 5,
                        CURLOPT_POSTFIELDS      => array (
                            'api_key' => "",
                            'file' => curl_file_create("temp/" . $hash . "/" . 'archive.zip', "application/zip", "temp/" . $hash . "/" . 'archive.zip'),
                            'title' => $title,
                            'validated' => $validated,
                            'channel' => $channel,
                            'ocr' => $ocr,
                            'detect_slides' => $slides
                        ) 
                    ) );
                    $result = curl_exec($ch);
                    $arr = json_decode($result, true);
                    if($arr['success'] == "1"){
                        $arr['success'] = "<i class='far fa-smile-beam'></i>";
                    }
                    else{
                        $arr['success'] = "<i class='far fa-sad-cry'></i>";
                    }
                    echo "<tr>";
                    echo "<td>" . $arr['slug'] . "</td>";
                    echo "<td>" . $arr['success'] . "</td>";
                    echo "</tr>";

                    curl_close($ch);
                }
            }
            echo "</tbody>";
            echo "</table>";
        }
    }
?>